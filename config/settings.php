<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Some settings
    |--------------------------------------------------------------------------
    |
    */
    'per_page' => (int)env('RECORDS_PER_PAGE'),
    'import_history_days' => (int)env('IMPORT_HISTORY_DAYS'),

    'bank' => [
        'given' => env('BANK_URL_GIVEN'),
        'url' => env('BANK_URL_XML')
    ],
];
