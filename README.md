##  TeT uzdevums
<ol>
<li><code>git clone git@gitlab.com:andris.ta/euro.git</code></li>
<li>Pārsauc <code>.env.example</code> uz <code>.env</code> un izmaina <code>APP_URL, DB_DATABASE, DB_USERNAME, DB_PASSWORD</code> parametrus </li>
<li>Konsolē izpilda <code>composer update</code></li>
<li>Konsolē izpilda <code>php artisan migrate</code></li>
<li>Ja vēlas ielādēt daudz vēsturiskos datus, konsolē izpilda <code>php artisan ImportRatesHistorical [dienu_skaits]</code></li>
</ol>
