<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'Frontend\SiteController@index')->name('index');
Route::post('/currency', 'Frontend\CurrencyController@post')->name('currency.post');
Route::get('/{currency}', 'Frontend\CurrencyController@show')->name('currency.show');
Route::get('/currency/{date}', 'Frontend\CurrencyController@date')->name('currency.date');
Route::get('/{currency}/download', 'Frontend\CurrencyController@download')->name('currency.download');
Route::get('/currency/downloadAll', 'Frontend\CurrencyController@downloadAll')->name('currency.downloadAll');
Route::get('/import/first', 'Frontend\SiteController@importFirst')->name('import.first');
