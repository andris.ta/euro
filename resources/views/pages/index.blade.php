<?php
use Carbon\Carbon;
use App\Models\Currency;
use App\Helpers\CurrencyHelper;
?>
@extends('layouts.main', ['title' => 'Sākums'])

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Valūtu kursi pret EUR</h1>
        @if($list->total())
            <a href="{{route('currency.downloadAll')}}" class="d-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Lejuplādēt .csv</a>
        @endif
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-6 mb-4 col-lg-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Šodienas datums</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{date('d.m.Y')}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 mb-4 col-lg-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Ierakstu skaits</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$list->total()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-chart-line fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 mb-4 col-lg-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Valūtu skaits</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$currencyList->count()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-piggy-bank fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    @if($list->count())
        <div class="card shadow mb-4 row">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rezultāti</h6>
            </div>
            <div class="card-body col-lg-8">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Valūtas nosaukums</th>
                            <th>Valūtas Kods</th>
                            <th>Datums</th>
                            <th>Kurss</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $item)
                            <tr>
                                <td>
                                    <a href="{{$item->url}}">{{CurrencyHelper::$names[$item->currency]}}</a>
                                </td>
                                <td>
                                    <a href="{{$item->url}}">{{$item->currency}}</a>
                                </td>
                                <td>
                                    {{$item->formatted}}
                                </td>
                                <td>
                                    {{$item->rate}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    @else

        <div class="alert alert-danger pb-0">
            <p>Nav datu, ko attēlot. Lūdzu, izlasiet README.</p>
            <p><a class="btn btn-primary" href="{{route('import.first')}}"><i class="fa fa-sync-alt"></i> Ielādēt datus no bank.lv</a></p>
        </div>

    @endif

    {{ $list->links() }}


@endsection
