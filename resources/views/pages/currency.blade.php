<?php
use App\Models\Currency;
use App\Helpers\CurrencyHelper;
?>
@extends('layouts.main', ['title' => $currency . ' vēsturiskie valūtu kursi', 'currency' => $currency])

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{CurrencyHelper::$names[$currency]}} ({{$currency}}) vēsturiskie valūtu kursi pret EUR</h1>

        @if($list->count())
            <a href="{{route('currency.download', ['currency' => $currency])}}"
               class="d-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Lejuplādēt .csv</a>
        @endif
    </div>



    @if($list->count())
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rezultāti</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Datums</th>
                            <th>Kurss</th>
                            <th>Izmaiņa</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $item)
                            <tr>
                                <td>
                                    {{$item->formatted}}
                                </td>
                                <td>
                                    {{$item->rate}}
                                </td>
                                <?php
                                $difference = false;
                                $difference = Currency::getChangeStatus($list[$loop->iteration] ? $list[$loop->iteration]->rate :  $item->rate, $item->rate);
                                ?>
                                <td class="text-{{$difference}}">
                                    @if($difference == 'success')
                                        <i class="fa fa-arrow-up"></i>
                                    @elseif($difference == 'danger')
                                        <i class="fa fa-arrow-down"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




        <div class="card shadow mb-4">

            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Kursa izmaiņu grafiks</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="currencyChart"></canvas>
                </div>
            </div>
        </div>


    @else
        <div class="alert alert-danger">
            Šai valūtai dati netika atrasti
        </div>

    @endif


    {{ $list->links() }}




@endsection
@section('scripts')
    @if($list->count())
        <?php
        $list = $list->reverse();

        ?>
        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>
        <!-- Page level custom scripts -->

        <script>
            // Set new default font family and font color to mimic Bootstrap's default styling
            Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,Roboto,"Helvetica Neue",Arial,sans-serif';
            Chart.defaults.global.defaultFontColor = '#858796';

            // Area Chart Example
            var ctx = document.getElementById("currencyChart");
            var myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["{!!implode('" , "', $list->pluck('formatted')->toArray())!!}"],
                    datasets: [{
                        label: "Kurss",
                        lineTension: 0.3,
                        backgroundColor: "rgba(78, 115, 223, 0.05)",
                        borderColor: "rgba(78, 115, 223, 1)",
                        pointRadius: 3,
                        pointBackgroundColor: "rgba(78, 115, 223, 1)",
                        pointBorderColor: "rgba(78, 115, 223, 1)",
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                        pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                        pointHitRadius: 10,
                        pointBorderWidth: 2,
                        data: [{{implode(' , ', $list->pluck('rate')->toArray())}}],
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 25,
                            top: 25,
                            bottom: 0
                        }
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: 'date'
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                            ticks: {
                                maxTicksLimit: 7
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 5,
                                padding: 10
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2]
                            }
                        }],
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        titleMarginBottom: 10,
                        titleFontColor: '#6e707e',
                        titleFontSize: 14,
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        intersect: false,
                        mode: 'index',
                        caretPadding: 10,
                        callbacks: {
                            label: function (tooltipItem, chart) {
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                return datasetLabel + ': ' + tooltipItem.yLabel;
                            }
                        }
                    }
                }
            });

        </script>
    @endif
@endsection
