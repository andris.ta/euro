<?php
use Carbon\Carbon;
use App\Models\Currency;
use App\Helpers\CurrencyHelper;
?>
@extends('layouts.main', ['title' => 'Valūtu kursi ' . Carbon::createFromFormat('Ymd', $date)->format('d.m.Y')])

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{'Valūtu kursi ' . Carbon::createFromFormat('Ymd', $date)->format('d.m.Y')}}</h1>
    </div>



    @if($list->count())
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Rezultāti</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Valūtas nosaukums</th>
                            <th>Valūtas Kods</th>
                            <th>Datums</th>
                            <th>Kurss</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $item)
                            <tr>
                                <td>
                                    <a href="{{$item->url}}">{{CurrencyHelper::$names[$item->currency]}}</a>
                                </td>
                                <td>
                                    <a href="{{$item->url}}">{{$item->currency}}</a>
                                </td>
                                <td>
                                    {{$item->formatted}}
                                </td>
                                <td>
                                    {{$item->rate}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    @else
        <div class="alert alert-danger">
            Šajā datumā nav valūtu kursu ierakstu.
        </div>

    @endif


    {{ $list->links() }}


@endsection
