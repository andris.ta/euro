<?php

use Carbon\Carbon;

?>
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ?? config('app.name') }}</title>
    <meta name="robots" content="noindex nofollow">
    <meta name="googlebot" content="noindex nofollow">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <base href="{{config('app.url')}}" />

    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="css/sb-admin-2.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
</head>

<body id="page-top">

<div id="wrapper">

    <div class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-tasks"></i>
            </div>
            <div class="sidebar-brand-text mx-3">Uzdevums</div>
        </a>

        <hr class="sidebar-divider my-0">

        <div class="nav-item  {{\Route::is('index') ? 'active' : ''}}">
            <a class="nav-link" href="/">
                <i class="fas fa-fw fa-home"></i>
                <span>Sākums</span></a>
        </div>

        @if($datesList->count())
            <ul class="list-unstyled">
                @foreach($datesList as $date)
                    <li class="nav-item {{\Route::is('currency.date', $date) && \Request::segment(2) == $date ? 'active' : ''}}">
                        <a class="nav-link py-1" href="{{route('currency.date', $date)}}"><i
                                class="fas fa-fw fa-calendar-alt"></i>
                            <span>{{Carbon::createFromFormat('Ymd', $date)->diffForHumans()}}</span>
                        </a></li>
                @endforeach
            </ul>
            <hr class="sidebar-divider my-0">
        @endif


        <hr class="sidebar-divider d-none d-md-block">

        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </div>


    <div id="content-wrapper" class="d-flex flex-column">


        <div id="content">


            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <!-- Topbar Search -->
                @if($currencyList->count())
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search"
                          method="post" action="{{route('currency.post')}}">
                        @csrf
                        <div class="input-group">

                            {{--<input type="text" class="form-control bg-light border-0 small" placeholder="Valūta" aria-label="Search" aria-describedby="basic-addon2">--}}

                            <select name="currency" id="currency-filter"
                                    class="form-control bg-light border-0 small border-0" required>
                                <option value="">Izvēlieties valūtu</option>
                                @foreach($currencyList as $item)
                                    <option value="{{$item}}"
                                            @if(isset($currency) && $currency == $item) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                @endif


            </nav>
            <!-- End of Topbar -->


            <!-- Begin Page Content -->
            <div class="container-fluid">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                    @if ($message = Session::get('error'))
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                @yield('content')
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>&copy; Andris T. {{date('Y')}}</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>


<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.js"></script>

@yield('scripts')
</body>

</html>
