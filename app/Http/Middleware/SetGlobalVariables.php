<?php

namespace App\Http\Middleware;

use App\Models\Currency;
use Carbon\Carbon;
use Closure;


class SetGlobalVariables
{


    public function handle($request, Closure $next, $guard = null)
    {
        //$list = Currency::where('date', Carbon::now()->subDays(1)->format('Ymd'))->get()->pluck('currency');
        $list = Currency::select('currency')->groupBy('currency')->get()->pluck('currency');
        view()->share('currencyList', $list);


        $dates = Currency::select('date')->groupBy('date')->latest('date')->limit(5)->get()->pluck('date');
        view()->share('datesList', $dates);

        return $next($request);
    }
}
