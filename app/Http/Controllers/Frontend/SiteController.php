<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use Egulias\EmailValidator\Exception\AtextAfterCFWS;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class SiteController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = Currency::latest('date')->paginate(config('settings.per_page'));

        return view('pages.index', compact('list'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function importFirst() {
        if(Currency::parseGivenXMl()) {
            return back()->with('success', 'Dati veiksmīgi ielādēti.');
        }
        else {
            return back()->with('error', 'Neizdevās ielādēt datus');
        }

    }


}
