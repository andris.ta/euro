<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\CurrencyHelper;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Http\Request;
use Carbon\Carbon;


class CurrencyController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function post(Request $request)
    {
        $request->validate([
            'currency' => 'required|min:3|max:3'
        ]);
        return redirect(route('currency.show', ['currency' => $request->currency]));
    }


    /**
     * @param Request $request
     * @param $currency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $currency) {
        $currency = strtoupper($currency);
        if(!isset(CurrencyHelper::$names[$currency])) {
            abort(404);
        }


        $list = Currency::where('currency', $currency)->latest('date')->paginate(config('settings.per_page'));
        return view('pages.currency', compact('currency', 'list'));
    }

    /**
     * @param Request $request
     * @param $currency
     */
    public function download(Request $request, $currency) : void {
        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename=' . $currency . '.csv');
        $list = Currency::select(['currency', 'date', 'rate'])->where('currency', $currency)->latest('date')->get()->toArray();
        $out = fopen('php://output', 'w');
        $headers = ['currency', 'date', 'rate'];
        fputcsv($out, $headers);
        if(!empty($list)) {
            foreach($list as $item) {
                $item['date'] = Carbon::createFromFormat('Ymd', $item['date'])->format('d.m.Y');
                fputcsv($out, $item);
            }
        }
        fclose($out);
    }

    /**
     * @param Request $request
     * @param $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function date(Request $request, $date) {
        try {
            Carbon::parse($date);
        } catch (\Exception $e) {
            abort(404);
        }
        $list = Currency::where('date', $date)->paginate(config('settings.per_page'));
        return view('pages.date', compact('date', 'list'));
    }


    /**
     *
     */
    public function downloadAll() : void {
        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename=all_rates.csv');
        $list = Currency::select(['currency', 'date', 'rate'])->latest('date')->get()->toArray();
        $out = fopen('php://output', 'w');
        $headers = ['currency', 'date', 'rate'];
        fputcsv($out, $headers);
        if(!empty($list)) {
            foreach($list as $item) {
                $item['date'] = Carbon::createFromFormat('Ymd', $item['date'])->format('d.m.Y');
                fputcsv($out, $item);
            }
        }
        fclose($out);
    }


}
