<?php

namespace App\Models;


use App\Helpers\CurrencyHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Currency
 *
 * @property int $id
 * @property string $date
 * @property string $currency
 * @property string $rate
 * @property string $created_at
 * @property string $formatted
 * @property string|null $unsubscribed_at
 * @mixin \Illuminate\Support\Facades\Eloquent
 */
class Currency extends Model
{

    public $timestamps = false;

    protected $table = 'currency';


    protected $fillable = ['date', 'currency', 'rate'];

    protected $dates = ['created_at'];

    protected $appends = ['formatted'];

    protected $casts = [
        'rate' => 'float'
    ];


    public static $arrContextOptions = [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false,
        ]
    ];

    /**
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return route('currency.show', ['currency' => $this->currency]);
    }

    /**
     * @return string
     */
    public function getFormattedAttribute(): string
    {
        return Carbon::createFromFormat('Ymd', $this->date)->format('d.m.Y');
    }


    /**
     * @return bool
     */
    public static function parseGivenXMl(): bool
    {
        $url = config('settings.bank.given');
        try {


            $response = file_get_contents($url, false, stream_context_create(Currency::$arrContextOptions));
            $data = json_decode(json_encode(simplexml_load_string($response)), true);

            if ($data && !empty($data['channel']) && !empty($data['channel']['item'])) {

                foreach ($data['channel']['item'] as $item) {
                    $list = CurrencyHelper::extractKeyValuePairs($item['description']);

                    if (!empty($list)) {
                        foreach ($list as $currency => $rate) {
                            Currency::updateOrCreate([
                                'date' => Carbon::parse($item['pubDate'])->format('Ymd'),
                                'currency' => $currency,
                                'rate' => $rate
                            ]);
                        }
                    }
                }
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * @param bool $date
     * @return bool
     */
    public static function parseBankXml($date = false): bool
    {
        if (!$date) {
            $date = date('Ymd');
        }

        $url = config('settings.bank.url') . '?date=' . $date;

        try {

            $response = file_get_contents($url, false, stream_context_create(Currency::$arrContextOptions));
            $data = json_decode(json_encode(simplexml_load_string($response)), true);


            if ($data && !empty($data['Date']) && !empty($data['Currencies']) && !empty($data['Currencies']['Currency'])) {

                foreach ($data['Currencies']['Currency'] as $currency) {
                    Currency::updateOrCreate([
                        'date' => $data['Date'],
                        'currency' => $currency['ID'],
                        'rate' => $currency['Rate']
                    ]);
                }
                return true;
            }
            return false;

        } catch (\Exception $e) {
            dd($e);
            return false;
        }
    }

    /**
     * @param $first
     * @param $second
     * @return string
     */
    public static function getChangeStatus($first, $second): string
    {
        $diff = '';
        if ($first < $second) {
            $diff = 'success';
        } elseif ($first > $second) {
            $diff = 'danger';
        }
        return $diff;
    }
}
