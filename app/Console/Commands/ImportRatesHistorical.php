<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Currency;

class ImportRatesHistorical extends Command
{

    public function __construct()
    {
        parent::__construct();
        DB::connection()->disableQueryLog();
    }


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportRatesHistorical {days?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import rates for previous X days';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($this->confirm('This will wipe all records. Do you want to continue?')) {

            Currency::truncate();

            $days = (int)$this->argument('days');

            if (empty($days)) {
                $days = config('settings.import_history_days');
                $this->warn('No days given. Using default ' . $days . ' day count.');
            }

            $this->info('Starting importing rates for last ' . $days . ' days');

            $bar = $this->output->createProgressBar($days);

            $success = 0;
            for ($i = $days; $i > 0; $i--) {
                if(Currency::parseBankXml(Carbon::now()->subDays($i)->format('Ymd'))) {
                    $success++;
                }
                $bar->advance();
            }

            $bar->finish();

            if($success != $days) {
                $this->error(PHP_EOL . 'Failed to retrieve rates for ' . ($days-$success) . ' of ' . $days . ' days.');
            }
            $this->info(PHP_EOL . 'Finished ' . date('Y-m-d H:i:s'));

        }


    }
}
