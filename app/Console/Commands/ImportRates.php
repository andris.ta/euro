<?php

namespace App\Console\Commands;


use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class ImportRates extends Command
{

    public function __construct()
    {
        parent::__construct();
        DB::connection()->disableQueryLog();
    }


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ImportRates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import daily rates';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Starting importing rates');

        if(!Currency::parseBankXml()) {
            $this->error(PHP_EOL . 'Failed to retrieve rates!');
        }

        $this->info(PHP_EOL . 'Finished ' . date('Y-m-d H:i:s'));


    }
}
